# -*- coding: utf-8 -*-
{
    'name': "Base Module",
    'version': '1.0',
    'depends': ['base'],
    'author': "Emmanuel Cruz",
    'category': 'Uncategorized',
    'description': """
        Modulo para establecer pies de pagina en los modulos sale order,
        account y picking
    """,
    # data files always loaded at installation
    'data': [
        'base_template.xml',
    ],
}
